import time
import board
import digitalio
import threading

print("press the buttons")

button1 = digitalio.DigitalInOut(board.PA1)
button1.direction = digitalio.Direction.INPUT

button2 = digitalio.DigitalInOut(board.PA0)
button2.direction = digitalio.Direction.INPUT

button3 = digitalio.DigitalInOut(board.PA3)
button3.direction = digitalio.Direction.INPUT

def funcEvent1():
  print("funciona 1")

def funcEvent2():
  print("funciona 2")

def funcEvent3():
  print("funciona 3")

def buttonEvent(button, func):
  INTERVAL = 1
  value = False
  lastTime = time.time()

  while True:
    if (not button.value and not value):
      if (time.time() - lastTime > INTERVAL):
        lastTime = time.time()
        func()

    value = not button.value
    time.sleep(0.0001)


threadBtn1 = threading.Thread(target=buttonEvent, args=(button1,funcEvent1,))
threadBtn1.start()
threadBtn2 = threading.Thread(target=buttonEvent, args=(button2,funcEvent2,))
threadBtn2.start()
threadBtn3 = threading.Thread(target=buttonEvent, args=(button3,funcEvent3,))
threadBtn3.start()
