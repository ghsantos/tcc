import time
from lib import bmp180
from lib.lcd import getLcd

# Initialise the LCD
lcd = getLcd()

while True:
  (temperature,pressure)=bmp180.readBmp180()
  atm = pressure * (1/1013.25)

  lcd.clear()
  lcd.message = "Temp: {0} C\nP: {1} atm".format(temperature, round(atm, 3))

  time.sleep(10)
