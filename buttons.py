import time
import board
import digitalio

print("press the buttons")

button1 = digitalio.DigitalInOut(board.PA1)
button1.direction = digitalio.Direction.INPUT

button2 = digitalio.DigitalInOut(board.PA0)
button2.direction = digitalio.Direction.INPUT

button3 = digitalio.DigitalInOut(board.PA3)
button3.direction = digitalio.Direction.INPUT

while True:
  print(button1.value, button2.value, button3.value)
  time.sleep(1)
