"""
https://telepot.readthedocs.io/en/latest/
https://github.com/datamachine/twx.botapi
"""

import telepot
from telepot.loop import MessageLoop
from twx.botapi import TelegramBot, ReplyKeyboardMarkup

botKey = 'botKey'

class Bot():
  def __init__(self, onData, onTurnsOnLed, onTurnsOffLed, onTurnsOnPump, onTurnsOffPump, onFile):
    super(Bot,self).__init__()
    self.onData = onData
    self.onTurnsOnLed = onTurnsOnLed
    self.onTurnsOffLed = onTurnsOffLed
    self.onTurnsOnPump = onTurnsOnPump
    self.onTurnsOffPump = onTurnsOffPump
    self.onFile = onFile
    self.bot = telepot.Bot(botKey)
    self.chatId = '281538215'

  def handle(self, msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    self.chatId = chat_id

    print(content_type, chat_type, chat_id)

    text = msg['text'].lower()
    print('Text: ', text)

    if('/start' in text):
        keyboard = [
          ['leitura atual', 'dados'],
          ['ligar led', 'desligar led'],
          ['ligar bomba', 'desligar bomba']
        ]
        reply_kb_markup = ReplyKeyboardMarkup.create(keyboard)
        self.bot.sendMessage(chat_id, "Iniciando...", reply_markup=reply_kb_markup)

    elif('leitura atual' in text):
      dados = self.onData()
      self.bot.sendMessage(chat_id, dados)

    elif('dados' in text):
      file = self.onFile()
      self.bot.sendDocument(chat_id, file)

    elif('desligar led' in text):
      self.onTurnsOffLed()
      self.bot.sendMessage(chat_id, "Led desligado")

    elif('ligar led' in text):
      self.onTurnsOnLed()
      self.bot.sendMessage(chat_id, "Led ligado")

    elif('desligar bomba' in text):
      self.onTurnsOffPump()
      self.bot.sendMessage(chat_id, "Bomba desligada")

    elif('ligar bomba' in text):
      self.onTurnsOnPump()
      self.bot.sendMessage(chat_id, "Bomba ligada")

    else:
      self.bot.sendMessage(chat_id, "Tente novamente!")

  def sendMessage(self, msg):
    print('message: ', msg)
    if (self.chatId):
      self.bot.sendMessage(self.chatId, msg)

  def sendDocument(self, doc):
    if (self.chatId):
      self.bot.sendDocument(self.chatId, doc)

  def start(self):
    MessageLoop(self.bot, self.handle).run_as_thread()
