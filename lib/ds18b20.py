# https://blog.ja-ke.tech/2019/01/21/DS18B20-armbian.html
# http://www.quemdevnaoteme.com/sensor-de-temperatura-dsb18b20-e-raspberry-pi-3/

# pin PD14

import os

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
temp_file = '/sys/bus/w1/devices/28-0308977923e0/w1_slave'

class DS18B20(object):
  """docstring for DS18B20."""

  def __init__(self):
    pass

  def read_temp_file(self):
    f = open(temp_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

  def read_temp(self):
    lines = self.read_temp_file()
    while lines[0].strip()[-3:] != 'YES':
      time.sleep(0.1)
      lines = self.read_temp_file()

    temp_output = lines[1].find('t=')
    if temp_output == -1:
      return -1

    temp_string = lines[1].strip()[temp_output+2:]
    temp_c = float(temp_string) / 1000.0
    return temp_c
