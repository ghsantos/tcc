import digitalio

class Rele(object):
  """docstring for Rele."""

  def __init__(self, pin):
    super(Rele, self).__init__()
    self.rele = digitalio.DigitalInOut(pin)
    self.rele.direction = digitalio.Direction.OUTPUT

  def __call__(self):
    return not self.rele.value

  def turnsOn(self):
    self.rele.value = False

  def turnsOff(self):
    self.rele.value = True

  def toggle(self):
    self.rele.value = not self.rele.value
