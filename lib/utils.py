import time

def hpaToAtm(hpa):
  return hpa * (1/1013.25)


TIME_FORMAT = '%H:%M'

def isTimeInterval(start, stop):
  localtime = time.strptime(time.strftime(TIME_FORMAT, time.localtime()), TIME_FORMAT)

  return localtime > time.strptime(start, TIME_FORMAT) and localtime < time.strptime(stop, TIME_FORMAT)
