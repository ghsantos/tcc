"""Get LCD lib"""
import board
import digitalio
import adafruit_character_lcd.character_lcd as characterlcd

# Modify this if you have a different sized character LCD
lcd_columns = 16
lcd_rows = 2

# Metro M0/M4 Pin Config:
lcd_rs = digitalio.DigitalInOut(board.PG7)
lcd_en = digitalio.DigitalInOut(board.PC3)
lcd_d7 = digitalio.DigitalInOut(board.PG6)
lcd_d6 = digitalio.DigitalInOut(board.PG9)
lcd_d5 = digitalio.DigitalInOut(board.PC7)
lcd_d4 = digitalio.DigitalInOut(board.PG8)
#lcd_backlight = digitalio.DigitalInOut(board.D13)

# Initialise the LCD class
def getLcd():
  lcd = characterlcd.Character_LCD_Mono(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6,
                                        lcd_d7, lcd_columns, lcd_rows)

  return lcd
