import time
import board
import digitalio

print("hello blinky!")

led = digitalio.DigitalInOut(board.PA21)
led.direction = digitalio.Direction.OUTPUT

on = False
off = True

while True:
    led.value = True
    time.sleep(0.5)
    led.value = False
    time.sleep(0.5)
