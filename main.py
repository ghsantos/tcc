import time
import board
import busio
import adafruit_bme280

from lib.bh1750 import BH1750
from lib.bme280 import BME280
from lib.ds18b20 import DS18B20
from lib.bot import Bot
from lib.rele import Rele
from lib.utils import hpaToAtm

"""
configurar sensores
configurar rele
configurar bot
"""

# Create the I2C interface.
i2c0 = busio.I2C(board.SCL, board.SDA)

luminance = BH1750(i2c0)
bmeTop = adafruit_bme280.Adafruit_BME280_I2C(i2c0, 0x76)
bmeBottom = adafruit_bme280.Adafruit_BME280_I2C(i2c0, 0x77)
bmeExternal = BME280()
pump = Rele(board.PA9)
led = Rele(board.PA8)
watherTemp = DS18B20()

bmeTop.sea_level_pressure = 1013.25
bmeBottom.sea_level_pressure = 1013.25

def onData():
  data = '''
luminosidade: {0} lúmens
topo:
--Temperatura: {1}°C
--umidade: {2}%
--Pressão: {3} atm
fundo:
--Temperatura: {4}°C
--umidade: {5}%
--Pressão: {6} atm
externo:
--Temperatura: {7}°C
--umidade: {8}%
--Pressão: {9} atm
Agua:
--Temperatura: {10}°C

Led {11}
Bomba {12}
  '''.format(round(luminance.luminance(), 2),
             round(bmeTop.temperature, 2),
             round(bmeTop.humidity, 2),
             round(hpaToAtm(bmeTop.pressure), 2),
             round(bmeBottom.temperature, 2),
             round(bmeBottom.humidity, 2),
             round(hpaToAtm(bmeBottom.pressure), 2),
             round(bmeExternal.temperature, 2),
             round(bmeExternal.humidity, 2),
             round(hpaToAtm(bmeExternal.pressure), 2),
             round(watherTemp.read_temp(), 2),
             'ligado' if led() else 'desligado',
             'ligada' if pump() else 'desligada')

  return data

def onTurnsOnLed():
  led.turnsOn()

def onTurnsOffLed():
  led.turnsOff()

def onTurnsOnPump():
  pump.turnsOn()

def onTurnsOffPump():
  pump.turnsOff()

def onFile():
  pass

bot = Bot(onData, onTurnsOnLed, onTurnsOffLed, onTurnsOnPump, onTurnsOffPump, onFile)
bot.start()

print('started')

while True:
  time.sleep(0.5)


# cooler = Rele(board.PA9)
#
# pump.turnsOn()
# print(pump())
# time.sleep(4)
# pump.turnsOff()
# print(pump())
#
# led.turnsOn()
# print(led())
# time.sleep(4)
# led.turnsOff()
# print(led())
#
