# https://www.filipeflop.com/produto/sensor-de-distancia-ultrassonico-hc-sr04/
# https://tutorials-raspberrypi.com/raspberry-pi-ultrasonic-sensor-hc-sr04/
# https://github.com/adafruit/Adafruit_CircuitPython_HCSR04

import time
import board
import adafruit_hcsr04

sonar = adafruit_hcsr04.HCSR04(trigger_pin=board.PC4, echo_pin=board.PC7)

while True:
    try:
        distance = sonar.distance
        print(distance)
        tank = 100 - ((distance-7) * 100 / 24)
        tankInterval = 0

        if (tank > 100):
          tankInterval = 100
        elif (tank < 0):
          tankInterval = 0

        print('test', tank)
        print('====', tankInterval)
    except RuntimeError as e:
        print(e)
        print("Retrying!")
    time.sleep(2)
