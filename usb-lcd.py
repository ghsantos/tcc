"""USB serial test using LCD"""
import time
import serial
from lib.lcd import getLcd

ser = serial.Serial('/dev/ttyUSB0', 9600)

# Initialise the LCD
lcd = getLcd()

while True:
  valor = ser.readline().decode().strip()

  lcd.clear()
  lcd.message = "usb: {0}".format(valor)

  time.sleep(1)
