import time
import board
import busio
import adafruit_bme280
import adafruit_hcsr04
import socketio

from lib.bh1750 import BH1750
from lib.bme280 import BME280
from lib.ds18b20 import DS18B20
from lib.rele import Rele
from lib.utils import hpaToAtm
from api.main import Api, URL
from lib.utils import isTimeInterval

sio = socketio.Client()

# Create the I2C interface.
i2c0 = busio.I2C(board.SCL, board.SDA)

luminance = BH1750(i2c0)
bmeTop = adafruit_bme280.Adafruit_BME280_I2C(i2c0, 0x76)
bmeBottom = adafruit_bme280.Adafruit_BME280_I2C(i2c0, 0x77)
bmeExternal = BME280()
sonar = adafruit_hcsr04.HCSR04(trigger_pin=board.PC4, echo_pin=board.PC7)
bump = Rele(board.PA8)
led = Rele(board.PA9)
secondaryLed = Rele(board.PA20)
# cooling = Rele(board.PA20) # PA10
watherTemp = DS18B20()

bmeTop.sea_level_pressure = 1013.25
bmeBottom.sea_level_pressure = 1013.25

def onSendData(send):
  watherReadTemp = 0
  readLuminance = 0
  distance = 31 # 0%
  error = False
  try:
    watherReadTemp = round(watherTemp.read_temp(), 2)
  except Exception as e:
    error = True
    print('watherReadTemp err:', e)
    pass

  try:
    readLuminance = round(luminance.luminance(), 2)
  except Exception as e:
    error = True
    print('readLuminance err:', e)
    pass

  try:
    distance = sonar.distance
  except Exception as e:
    error = True
    print('distance err', e)
    pass

  # 31 => 0%
  # 7 => 100%
  tank = 100 - ((distance-7) * 100 / 24)
  print('tank', tank)
  if (tank > 100):
    tank = 100
  elif (tank < 0):
    tank = 0

  data = {
    "luminance": readLuminance,
    "topTemperature": round(bmeTop.temperature, 2),
    "topHumidity": round(bmeTop.humidity, 2),
    "bottomTemperature": round(bmeBottom.temperature, 2),
    "bottomHumidity": round(bmeBottom.humidity, 2),
    "externalTemperature": round(bmeExternal.temperature, 2),
    "externalHumidity": round(bmeExternal.humidity, 2),
    "waterTemperature": watherReadTemp,
    "led": led(),
    "bump": bump(),
    "tank": tank
  }

  try:
    send(data)
  except Exception as e:
    error = True
    print('send data err:', e)
    pass

  return error


MAX_TEMP = 45
MIN_TEMP = 40
def verifyWarning(warning):
  try:
    topTemperature = bmeTop.temperature
    bottomTemperature = bmeBottom.temperature
    externalTemperature = bmeExternal.temperature
    if (topTemperature > MAX_TEMP or bottomTemperature > MAX_TEMP or
    externalTemperature > MAX_TEMP):
      return True

    if (warning and (topTemperature > MIN_TEMP or bottomTemperature > MIN_TEMP or
    externalTemperature > MIN_TEMP)):
      return True
  except Exception as e:
    print('verifyWarning err', e)
    return False

  return False


TIME_DAY_START = '5:00'
TIME_DAY_STOP = '21:00'

TIME_SECONDARY_LED_START = '7:00'
TIME_SECONDARY_LED_STOP = '19:00'

TIME_ON_BUMP = 15 * 60 # ligado 15 minutos
TIME_OFF_BUMP_DAY = 30 * 60 # desligado 30 minutos
TIME_OFF_BUMP_NIGHT = 2 * 60 * 60 # desligado 2 HORAS

warning = False
bumpDisable = False
ledDisable = False
# coolingDisable = False
bumpTime = time.time()

def actuatorsControl(sendMessage):
  global bumpTime
  global warning
  global bumpDisable
  global ledDisable
  warning = verifyWarning(warning)

  if (warning):
    bump.turnsOff()
    led.turnsOff()
    secondaryLed.turnsOff()

    sendMessage('temperatura alta')
  else:
    isDayTime = isTimeInterval(TIME_DAY_START, TIME_DAY_STOP)

    if (isDayTime):
      if (not led() and not ledDisable):
        led.turnsOn()
        sendMessage('led ligado')
    else:
      if (led()):
        led.turnsOff()
        sendMessage('led desligado')

    if (isTimeInterval(TIME_SECONDARY_LED_START, TIME_SECONDARY_LED_STOP)):
      if (not secondaryLed() and not ledDisable):
        secondaryLed.turnsOn()
        sendMessage('secondaryLed ligado')
    else:
      if (secondaryLed()):
        secondaryLed.turnsOff()
        sendMessage('secondaryLed desligado')

    nextTime = time.time()
    timeBumpOff = TIME_OFF_BUMP_DAY if isDayTime else TIME_OFF_BUMP_NIGHT

    if (bump() and nextTime > bumpTime + TIME_ON_BUMP):
      bumpTime = nextTime
      bump.turnsOff()
      sendMessage('bomba desligada')
    elif (not bump() and not bumpDisable and nextTime > bumpTime + timeBumpOff):
      bumpTime = nextTime
      bump.turnsOn()
      sendMessage('bomba ligada')

  # if (coolingDisable):
  #   cooling.turnsOff()
  # else:
  #   cooling.turnsOn()


print('started')

cycleTime = time.time()
cycleDuration = 60
api = Api()

def sendMessage(text):
  print('message:', text)

@sio.on('bumpDisable')
def on_message(data):
  global bumpDisable
  bumpDisable = data
  print('on bumpDisable', data)

  if (bumpDisable):
    bump.turnsOff()


@sio.on('ledDisable')
def on_message(data):
  global ledDisable
  ledDisable = data
  print('on ledDisable', data)

  if (ledDisable):
    led.turnsOff()
    secondaryLed.turnsOff()


# @sio.on('coolingDisable')
# def on_message(data):
#   global coolingDisable
#   coolingDisable = data
#   print('on coolingDisable', data)
#
#   if (coolingDisable):
#     cooling.turnsOff()


sio.connect(URL)


bump.turnsOn()
# cooling.turnsOn()

while True:
  nextTime = time.time()

  actuatorsControl(sendMessage)

  if (nextTime > cycleTime + cycleDuration):
    onSendData(api.sendData)
    formatedTime = time.strftime("%d/%m/%Y %H:%M:%S", time.localtime())
    print(formatedTime)
    cycleTime = nextTime

  time.sleep(0.5)
