import time
import board
import busio
import adafruit_bme280

from lib.bh1750 import BH1750
from lib.bme280 import BME280
from lib.ds18b20 import DS18B20
from lib.bot import Bot
from lib.rele import Rele
from lib.utils import hpaToAtm

"""
configurar sensores
configurar rele
configurar bot
"""

# Create the I2C interface.
i2c0 = busio.I2C(board.SCL, board.SDA)

luminance = BH1750(i2c0)
bmeTop = adafruit_bme280.Adafruit_BME280_I2C(i2c0, 0x76)
bmeBottom = adafruit_bme280.Adafruit_BME280_I2C(i2c0, 0x77)
bmeExternal = BME280()
pump = Rele(board.PA9)
led = Rele(board.PA8)
watherTemp = DS18B20()

bmeTop.sea_level_pressure = 1013.25
bmeBottom.sea_level_pressure = 1013.25

def onData():
  watherReadTemp = ''
  readLuminance = ''
  try:
    watherReadTemp = round(watherTemp.read_temp(), 2)
    readLuminance = round(luminance.luminance(), 2)
  except Exception as e:
    print(e)
    pass

  data = '''
luminosidade: {0} lúmens
topo:
--Temperatura: {1}°C
--umidade: {2}%
--Pressão: {3} atm
fundo:
--Temperatura: {4}°C
--umidade: {5}%
--Pressão: {6} atm
externo:
--Temperatura: {7}°C
--umidade: {8}%
--Pressão: {9} atm
Agua:
--Temperatura: {10}°C

Led {11}
Bomba {12}
  '''.format(readLuminance,
             round(bmeTop.temperature, 2),
             round(bmeTop.humidity, 2),
             round(hpaToAtm(bmeTop.pressure), 2),
             round(bmeBottom.temperature, 2),
             round(bmeBottom.humidity, 2),
             round(hpaToAtm(bmeBottom.pressure), 2),
             round(bmeExternal.temperature, 2),
             round(bmeExternal.humidity, 2),
             round(hpaToAtm(bmeExternal.pressure), 2),
             watherReadTemp,
             'ligado' if led() else 'desligado',
             'ligada' if pump() else 'desligada')

  return data


TITLE = 'Luminosidade;T Temperatura;T Umidade;T Pressao;F Temperatura;F Umidade;F Pressao;E Temperatura;E Umidade;E Pressao;A Temperatura;Led;Bomba;Data;UTC\n'

def saveData(data):
  arq = open('homologacao.csv', 'a+')

  if (arq.tell() == 0):
    arq.write(TITLE)

  arq.write(data)
  arq.close()


def onSaveData():
  now = time.time()
  formatedTime = time.strftime("%d/%m/%Y %H:%M:%S", time.localtime())

  watherReadTemp = ''
  readLuminance = ''
  error = False
  try:
    watherReadTemp = round(watherTemp.read_temp(), 2)
    readLuminance = round(luminance.luminance(), 2)
  except Exception as e:
    error = True
    print(e)
    pass

  data = '''{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14}\n'''.format(readLuminance,
            round(bmeTop.temperature, 2),
            round(bmeTop.humidity, 2),
            round(hpaToAtm(bmeTop.pressure), 2),
            round(bmeBottom.temperature, 2),
            round(bmeBottom.humidity, 2),
            round(hpaToAtm(bmeBottom.pressure), 2),
            round(bmeExternal.temperature, 2),
            round(bmeExternal.humidity, 2),
            round(hpaToAtm(bmeExternal.pressure), 2),
            watherReadTemp,
            'ligado' if led() else 'desligado',
            'ligado' if pump() else 'desligado',
            formatedTime,
            now)

  saveData(data)
  return error

def onFile():
  arq = open('homologacao.csv', 'rb')
  return arq

def onTurnsOnLed():
  led.turnsOn()

def onTurnsOffLed():
  led.turnsOff()

def onTurnsOnPump():
  pump.turnsOn()

def onTurnsOffPump():
  pump.turnsOff()


MAX_TEMP = 45
MIN_TEMP = 40
def verifyWarning(warning):
  try:
    if (bmeTop.temperature > MAX_TEMP or bmeBottom.temperature > MAX_TEMP or
    bmeExternal.temperature > MAX_TEMP):
      return True

    if (warning and (bmeTop.temperature > MIN_TEMP or bmeBottom.temperature > MIN_TEMP or
    bmeExternal.temperature > MIN_TEMP)):
      return True
  except Exception as e:
    print(e)
    return False

  return False


TIME_FORMAT = '%H:%M'
TIME_LED_START = '6:00'
TIME_LED_STOP = '18:00'

TIME_ON_BUMP = 30 * 60 # 30 minutes
TIME_OFF_BUMP = 10 * 60 # 10 minutes

warning = False
pumpTime = time.time()

def actuatorsControl(sendMessage):
  global pumpTime
  global warning
  warning = verifyWarning(warning)

  if (warning):
    pump.turnsOff()
    led.turnsOff()

    sendMessage('temperatura alta')
  else:
    if (time.strptime(time.strftime(TIME_FORMAT, time.localtime()), TIME_FORMAT) > time.strptime(TIME_LED_START, TIME_FORMAT)
      and time.strptime(time.strftime(TIME_FORMAT, time.localtime()), TIME_FORMAT) < time.strptime(TIME_LED_STOP, TIME_FORMAT)):
      if (not led()):
        led.turnsOn()
        sendMessage('led ligado')
    else:
      if (led()):
        led.turnsOff()
        sendMessage('led desligado')

    nextTime = time.time()
    if (pump() and nextTime > pumpTime + TIME_ON_BUMP):
      pumpTime = nextTime
      pump.turnsOff()
      sendMessage('bomba desligada')
    elif (not pump() and nextTime > pumpTime + TIME_OFF_BUMP):
      pumpTime = nextTime
      pump.turnsOn()
      sendMessage('bomba ligada')


bot = Bot(onData, onTurnsOnLed, onTurnsOffLed, onTurnsOnPump, onTurnsOffPump, onFile)
bot.start()

print('started')

cycleTime = time.time()
cycleDuration = 60


pump.turnsOn()
while True:
  nextTime = time.time()

  if (nextTime > cycleTime + cycleDuration):
    saveError = onSaveData()
    cycleTime = nextTime
    messageError = '\nErro na leitura' if saveError else ''

    try:
      bot.sendMessage('saved data' + messageError)
    except Exception as e:
      print('sendMessage error')
      print(e)

  actuatorsControl(bot.sendMessage)

  time.sleep(0.5)

'''
warning == true

state cycle
warning ? normal = off :
lampada -> 6:00 - 18:00

bomba -> 30 min ligada - 10 desligada
'''
