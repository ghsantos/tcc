# https://blog.ja-ke.tech/2019/01/21/DS18B20-armbian.html
# http://www.quemdevnaoteme.com/sensor-de-temperatura-dsb18b20-e-raspberry-pi-3/

# pin PD14

import time

from lib.ds18b20 import DS18B20

ds18b20 = DS18B20()

while True:
  print(ds18b20.read_temp())
  time.sleep(1)
