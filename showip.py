#!/usr/bin/env python3

import orangetool
import time

from lib.lcd import getLcd

# Initialise the LCD
lcd = getLcd()

lcd.message = 'starting...'

time.sleep(5)

lcd.clear()
lcd.message = 'ip:\n' + orangetool.local_ip()
