from busio import I2C
from board import SCL, SDA
from lib.bh1750 import BH1750
import time

# Create the I2C interface.
i2c = I2C(SCL, SDA)
i2c.try_lock()
s = BH1750(i2c)

while True:
  print(s.luminance())
  time.sleep(2)
